//
//  MVVM_ExampleApp.swift
//  MVVM_Example
//
//  Created by Ravi Prajapat on 25/06/22.
//

import SwiftUI

@main
struct MVVM_ExampleApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
